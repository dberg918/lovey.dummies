---
layout: page
title: Contact us
description: Send us a love note!
---

## Send us a love note
Would you like to give us feedback? Do you have a story or a poem you'd like to 
share? Use the form below to contact us!

<form action="//formspree.io/lovenotes@loveydummies.com" method="POST">
  <p>
    <input type="text" name="name" placeholder="Name" />
  </p>

  <p>
    <input type="email" name="_replyto" placeholder="Email address" />
  </p>

  <p>
    <input type="hidden" name="_subject" value="Love note from loveydummies.com" />
  </p>

  <p>
    <textarea name="message" maxlength="4000" placeholder="Your note*" required></textarea>
  </p>

  <p>
    <input type="hidden" name="_next" value="/submitted" />
  </p>

  <p>
    <input type="text" name="_gotcha" style="display:none" />
  </p>

  <p class="required">
  * Denotes required fields
  </p>

  <center>
    <input class="button" type="submit" value="&#xf1d8; Send" />
    <input class="button" type="reset" value="&#xf1f8; Discard" />
  </center>
</form>

