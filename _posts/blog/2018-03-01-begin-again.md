---
layout:     blog
title:     "Begin Again"
date:       2018-03-01 15:31:00 +0900
---

The plum blossoms at Osaka Castle park are already out to herald my favorite 
season!

It is still cold but the winter trees no longer shiver under the mild sunlight. 
I marvel in the thought that just over a month ago these trees looked like the 
hands of an old man *in extremis* reaching to heaven---either surrendering to a 
peaceful death or pleading for life. And today, I witnessed something like an 
answered prayer---life has begun again.

> "In winter, I plot and plan. In spring, I move."  
>  \- Henry Rollins

The first few weeks of the new year have always been a struggle for me. Every 
year I long for a resolution---to improve myself more, to redeem all the times 
I've wasted in the past years, and to make the new year my best year yet. And 
still, the first few weeks have been a disappointing trial and error period for 
new habits I'm trying to imbibe. It doesn't help that it's arduous to get out 
of bed in the morning and the sky weighs me down with its many shades of gray.

But as soon as spring rain starts to fall instead of snow and the buds come 
bursting forth, it's as if all of life is extending an invitation for me to 
spring from dejection and begin again.

Today is the first day of March and it feels like the first of all the days of 
spring this year! I'm looking at the list of many things I'd like to 
do---articles to write, interviews to have, fun times, lovey times!

Let's begin again today!

Cheers!

Pearl
{: .sig}
