---
layout:     blog
title:     "What Happened?!"
date:       2017-12-31 15:18:00 +0900
---

And so here comes the last day of 2017. Suddenly, I remembered 50 more things I 
should’ve done yesterday. But oh, the page will soon be turned whether I like it 
or not. “Time and tide waits for no one.” No one.

## Waaaaiiiitttt!!!

As soon as the final count down begins for most people, I would schlep myself 
back to the past 364 and a quarter days… revisiting all the highs and lows, the 
good, the bad and the crazy with a list of questions, hoping that my answers 
will make me wiser.

Before the end of every year, I need to have my special “me-time.” It has become 
a personal tradition with my Bible and my journal. I would lock myself in my 
room (like a weirdo) while everyone else is eating and drinking, to give the 
year one more glance before I say adieu. It’s just that I believe each year 
deserves a clean closure, an end mark. Period. 

Then I would plead God for His loving-kindness, blessings and protection for me 
and my loved ones in the coming year. Only until then I can let 2017 go...

## Whatever happened, happened

One thing I’m thankful about the passage of time is the closure it brings. The 
year 2017 is ending and it’s a gift to many. To some it has been great but to 
others it has been a year in the desert with every single day parching their 
existence to their last breath. And 2018 is a relief, a refresh. A new year 
gives us something new---a new beginning.

So what if I lost a job in 2017? 2018 is prancing in giving me the career I’ve 
always wanted, thank you very much!

I know friends who had suffered loses of loved ones this year like Menchie and 
Ate Jhen. Even my own extended family lost an Alatiit sweetheart. And still, 
none of what happened caught God by surprise for He has made all things 
beautiful in His time.

> When times are good, be happy; but when times are bad, consider this: 
> God has made the one as well as the other...  
> -- Ecclesiastes 7:14


Whatever happened to you in 2017, I hope that you had a blast during the good 
times, partied hard on your milestones and achievements, grieved well in times 
of loss, cried yourself to peace in times of sadness and hardships and lived 
life to the fullest.

## Walk by faith

In just a few hours, we will end this year. We will leave the past to the past 
and welcome the new. 

> ...Therefore, no one can discover anything about their future.  
> -- Ecclesiastes 7:14

And since we cannot discover anything about our future (thank God!), we can 
trust that He who promised is faithful. We can hope for the best and prepare for 
the worst. And know for sure that God loves us with an everlasting love.

I used to get anxious when the year begins because it bring so much uncertainty. 
But the truth is a huge part of the uncertainty is possibility! Possibilities we 
can realize if only we learn how to faith it till we make it. So, shall we?

But first, 

*2017, you are finished.*

Pearl
{: .sig}
