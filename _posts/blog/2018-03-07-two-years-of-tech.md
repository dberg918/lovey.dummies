---
layout:     blog
title:     "Two Years of Tech"
date:       2018-03-07 22:54:16 +0900

photoCredit_name:  israel palacio
photoCredit_url:   https://unsplash.com/photos/Y20JJ_ddy9M
---

It's hard to believe, but we are winding up our second year of podcasting with 
Lovey Dummies this week. For me, it has been a great learning experience and a 
lot of fun.

Pearl and I have said that we started this podcast as a way to 
reassure listeners that they have company in their trials when it comes to 
relationships, but personally, I wanted to make it to reassure myself! We have 
had many moments wherein we felt pretty isolated in terms of shared experiences, 
so it has been eye-opening for us to talk with our friends about their 
perspectives and the things they've gone through.

However, I would be remiss if I didn't talk about the technical side of things, 
as I am the podcast's resident nerd-in-chief. Here's a rundown of what we've 
done to improve the listening experience and the back-end infrastructure of 
Lovey Dummies this past year.

## Chapters
Our podcast tends to run about 20--30 minutes on average (sometimes we near the 
hour mark!), which means it can be difficult to share snippets of our show with 
your friends. At the beginning of 2017, we introduced chapter support for our 
Apple Podcasts feed, which allows users to skip through the episode to specific 
topics or questions of interest. Those of you who use Pocket Casts or Overcast 
are probably familiar with this feature. If you're using Apple's standard 
Podcasts app, we recommend switching to an app that has chapter support, or just 
use this website!

Integrating support for chapters involved learning how to create them in 
Audacity, our audio editor, as well as figuring out how to write them into an 
audio file. As a result, we coded [chappy][ch], a small script that reads in 
exported Audacity labels and converts them to a format suitable for our audio 
files, as well as WebVTT format for our website.

## Audio player
As a part of our move to include chapters in our episodes, we also hacked 
together a custom audio player for the website that would allow listeners to 
access those chapters in a web browser. We aren't exactly Javascript experts, so 
we would welcome your help in [improving it][pjs]!

## Site redesign
In July, we tweaked how our episodes appear on the website to make them a little 
more personal. You can see a picture of our interviewee(s) now while you listen!

## Post-production
Our post-production process has changed a bit over the course of time, and 
that's part of why we were able to increase production in our second year. It is 
mostly due to experience---despite still making a ton of mistakes in our 
recordings, we're getting better at identifying them after the fact and pulling 
them out in the finished product. One helpful tip was learning to snap or clap 
at cut points to make them easier to spot. There is certainly still room for 
improvement, but I am happy that episode quality seems to be on an upward trend.

Probably the most significant change, however, was a [shell script][le] I wrote 
to automate most of the launch process for each episode. Instead of looking up 
a reference guide for all the steps involved in getting the audio files ready, 
all I have to do is invoke the script with the title and episode number, and 
voilà! The audio is compressed, tagged, and uploaded to the Internet for 
consumption. It also prepares a show notes file with some front matter that is 
very important to our iTunes feed. No more hunting on Google for esoteric shell 
commands!

## Going forward
We are always trying to make our podcast better, whether it's improving our 
back-end workflows or searching for interesting stories. Personally, I am hoping 
to find time this year to rewrite our website's audio player to make it more 
robust and add the ability to increase playback speed. I'm also hoping to learn 
and implement new ways to boost the quality of our audio, whether we are 
interviewing our guests in person or across the ocean. Pearl and I have also 
talked about playing with the format of our show---we'll see if anything 
interesting falls out!

It has been a great two years of producing and creating, and we'd like to thank 
all of our listeners and subscribers for sticking with us. We appreciate your 
listenership and we hope you will enjoy what we have in store for 2018!

Dave
{: .sig}

[ch]:    https://github.com/dberg918/automation.scripts/blob/master/chappy
[pjs]:   https://gitlab.com/dberg918/lovey.dummies/tree/master/player
[le]:    https://github.com/dberg918/automation.scripts/blob/master/launch-episode
