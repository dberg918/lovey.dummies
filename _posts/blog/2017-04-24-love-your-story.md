---
layout:      blog
title:      "Love Your Story"
date:        2017-04-24 20:43:42 +0800
---

I have a confession to make: every now and then, the green monster forces its
way into my heart and devours the sunshine on my perfect day. It typically
happens when I get flooded by images and stories on social media of lives unlike
mine, or like when I remember my friend, Bambi.

Bambi is very classy and feminine unlike me, who’s more like a leader of a tribe
in a Southeast Asian jungle. Bambi is soft spoken and lovely in frame,
fair-skinned and has a patented charming smile. All those using filters (we know
who you are, by the way; no offense meant, we love you anyway) will be put to
shame by every snapshot she posts. With her long black hair, she could easily
get a shampoo commercial deal.

While she was in the Philippines, Bambi’s then-boyfriend was living in Paris. He
is gorgeous, young, rich, and intelligent like her. He invited Bambi to Paris
and asked her to spend the rest of her life with him, with the stars, moon, the
Eiffel Tower, and a very heavy diamond ring as their witnesses.

Don’t get me wrong, I was genuinely happy for her and she even asked if I could
fly to France to be a bridesmaid. I was thrilled and did my best to go, but the
timing wasn’t right and it didn’t work out. But I got every photo of her wedding
day.

So every time I think about a “fairy tale love story,” I always think about
Bambi and how easy and sweet her life is. But sometimes, it backfires and envy
steals, kills, and destroys my joy and peace. I begin to question how unfair
life and God is. Regrets condemn me when I look in the mirror---all because of
another person’s happy life.

## Creating Stories

I stumbled upon an article about a woman who cropped and stole a photo of
someone else’s fianc&eacute; and made a twitter account for him (for a more convincing
social proof) and went through lengths to convince her friends and family that
she is, in fact, engaged to be married to the innocent stranger. She got caught
and turned into a laughing stock and another image of pathetic pity.

Related to that article is another recent discovery to me. There’s an app that
allows you to create your own virtual boyfriend/girlfriend. Unlike Siri, the
other person is working for the app company and responds like a real lover. I
thought it could be an interesting job! But kidding aside, it broke my heart as
it exposed yet more deep-rooted diseases of the society---loneliness, fear,
envy, and discontent.


## Love Your Story

Even before I met my future husband, I already knew my life was already like a
far, far away galaxy where Bambi’s fairy tale is taking place. I had learned to
accept that I have made mistakes in the past and I am simply not like her. I
only hoped to have at least half of what she had in life.

When I began to love my “here and now,” and live in the present, I began to be
contented in whatever circumstance I am currently in---broke, struggling,
working two jobs, single mom, etc. I met people who are like me. I found comfort
that I am not alone; some women are also raising their kids by themselves; some
have never had a relationship in the past couple of decades; some were even just
starting over with life at 50.

It was a slow process for me, but I realized that God wrote our story
differently. And we are free to make choices. We are allowed to make mistakes
and God reaches out to us, picks us up in His mercy, and gives us sufficient
grace to start where we are. But I'm not gonna lie, it isn't easy. It takes
humility to accept one's lot in life---it goes against human nature.

The truth is, the *real* us is more than loveable; the Maker of the Universe
thinks we are to die for. Although it is a painful journey to get there and to
some of us, it could be a long journey, nothing compares to being loved for who
you really are. Let me end with a quote from Tim Keller:

> To be loved but not known is comforting but superficial. To be
> known and not loved is our greatest fear. But to be fully known and
> truly loved is, well, a lot like being loved by God. It is what we
> need more than anything. It liberates us from pretense, humbles us
> out of our self-righteousness, and fortifies us for any difficulty
> life can throw at us. --*The Meaning of Marriage*

So, [what’s your story?][em]

Pearl
{: .sig}

[em]: http://loveydummies.com/contact
