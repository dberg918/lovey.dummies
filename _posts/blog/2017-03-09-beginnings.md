---
layout:     blog
title:     "Beginnings"
date:       2017-03-09 22:03:28 +0900
---

It all began with a crazy idea: What if we started our own podcast?

Since Dave introduced me to podcasts a couple of years ago, it has changed my
life forever. Just kidding! Not really. Let’s just say it added sparkles to my
commutes and alone times. It astounded me to find out that there’s a ton of
podcasts for almost everything! Dave just got me started with one about the
“not-so-good" Christians. I enjoyed how they view Christianity in the here and
now, as 21st-century dwellers. Then I discovered another one that indulged my
hunger for poetry. Why did I discover the poetry reading podcast only now?! And
I kept discovering more and more...

Then we saw a need. I got many friends asking me questions regarding their
relationship endeavors. I’m not a relationship expert but I guess I’m a wide
reader which compensates to the missing areas of my wide range of experiences.
And since I’m genuinely interested in people, it was a delight to listen to
whatever my friends were going through and help them through it.

So that’s pretty much what Lovey Dummies looked like as a brainfetus! And now,
Lovey Dummies is one year old! YAY!!!

OMG! It’s hard to believe that despite it being a busy year for both Dave and
I, God has lead us to truth speakers (as we call them); those courageous
individuals who talked real and shared a piece of themselves to let others know
that they are not alone in their situation.

In our show, we do our best to listen intently to people. We do not invite
coaches and counselors (unless they are willing to talk about their own personal
experiences) because we would like our show to be an anthology of real life and
love experiences. We want to talk to people who are willing to share their
stories raw. #nofilter

In our second year, we have decided to commence the Lovey Dummies blog. We
invite you to [share your stories][em] with us and join our discussions on
[Twitter][tw] and [Instagram][in]. Feel free to ask questions and meet people in
the same boat as you.

Dave and I are excited for more date nights, talks and real stories with you!

Pearl
{: .sig}

[em]: http://loveydummies.com/contact "Love Notes"
[tw]: https://twitter.com/loveydummies "@loveydummies on Twitter"
[in]: https://instagram.com/loveydummies "@loveydummies on Instagram"
