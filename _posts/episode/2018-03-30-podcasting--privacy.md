---
layout:    episode
title:    "Podcasting & Privacy"
subtitle:  Special
date:      2018-03-30 18:40:34 +0900
itunes:    episode-24.m4a
opus:      episode-24.opus
chapters:  episode-24.vtt
length:    7753535
duration: "14:09"
---

What are the boundaries for sharing personal details on a podcast? In part 2 of 
our 2nd anniversary special, we look back at the stories we have covered and 
discuss whether we should share more of our own personal experiences.

Be sure to get in touch if you want to talk with us!

 - [Instagram][ig]
 - [Twitter][tw]
 - [Contact form][cf]

[ig]: https://instagram.com/loveydummies
[tw]: https://twitter.com/loveydummies
[cf]: https://loveydummies.com/contact
