---
layout:    episode
title:    "Finances for Engaged Couples"
subtitle:  Date Night
date:      2018-07-16 12:57:19 +0900
itunes:    episode-26.m4a
opus:      episode-26.opus
chapters:  episode-26.vtt
length:    9525364
duration: "17:48"

photoCredit_name:  rawpixel
photoCredit_url:   https://pixabay.com/en/paper-business-aerial-view-benefit-3190198/
---

Being engaged is an exciting time for couples, but how should you approach the 
touchy subject of money? Join us on this date night for a discussion about how 
couples about to tie the knot can get a leg up on their finances.

### Sources
[11 Ways Engaged Couples Should Deal with Finances Now][fi], by Deepak Reju

[fi]: https://www.thegospelcoalition.org/article/11-ways-engaged-couples-should-deal-with-finances-now/
