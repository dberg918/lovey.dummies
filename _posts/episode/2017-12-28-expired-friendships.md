---
layout:    episode
title:    "Expired Friendships"
subtitle:  Marianna & Keli
date:      2017-12-28 11:39:58 +0900
itunes:    episode-21.m4a
opus:      episode-21.opus
chapters:  episode-21.vtt
length:    29360937
duration: "58:38"
---

Sometimes, you have to let go. How do we know when a relationship has run its 
course, or when it's worth our time to hang on and stay in touch? Join us as 
we talk with Marianna and Keli about the ebb and flow of friendships.
