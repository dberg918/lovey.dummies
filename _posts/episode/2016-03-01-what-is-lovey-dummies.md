---
layout:    episode
title:    "What is Lovey Dummies?"
subtitle:  Podcast introduction
date:      2016-03-01 21:14:00 +0900
itunes:    episode-1.mp3
opus:      episode-1.ogg
length:    4001792
duration: "5:10"
---

In our first episode, we introduce the podcast and ourselves. Let's get started!

