---
layout:    episode
title:    "Women Dating in a Foreign Country"
subtitle:  Wesam
date:      2016-04-20 20:42:00 +0900
itunes:    episode-3.mp3
opus:      episode-3.opus
length:    22910976
duration: "31:26"
---

What is it like to date in a foreign country? Let's take a glimpse at a single 
woman's life with our guest from the Middle East.

