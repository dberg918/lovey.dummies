---
layout:    episode
title:    "Leaving a Long-Term Relationship"
subtitle:  Chris
date:      2016-08-28 15:22:00 +0900
itunes:    episode-6.mp3
opus:      episode-6.opus
length:    16689152
duration: "22:47"
---

Breakups are never easy, especially when it's a long-term relationship. Join 
us as our friend Chris shares the end of his 7-year relationship when he left 
his home country.

