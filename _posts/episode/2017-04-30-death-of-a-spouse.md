---
layout:    episode
title:    "Death of a Spouse"
subtitle:  Menchie
date:      2017-04-30 17:39:36 +0800
itunes:    episode-13.m4a
opus:      episode-13.opus
chapters:  episode-13.vtt
length:    39296132
duration: "79:06"
---

Losing a spouse is devastating, especially after just a few years of marriage. 
Join us and our friend Menchie as she tells us the story of Odelon, her late 
husband who died suddenly of brain cancer at age 31.
