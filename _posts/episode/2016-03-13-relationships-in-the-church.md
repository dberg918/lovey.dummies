---
layout:    episode
title:    "Relationships in the Church"
subtitle:  Seth & Patty
date:      2016-03-13 17:16:00 +0900
itunes:    episode-2.mp3
opus:      episode-2.ogg
length:    21942272
duration: "30:05"
---

How do you handle romantic situations in a church? Join us as Seth from Ghana 
and Patty from Indonesia tell us their story.

#### Commentary sources
[*Choosing God's Best*][1], by Dr. Don Raunkiar

[1]: https://www.amazon.com/Choosing-Gods-Best-Lifelong-Romance/dp/1590524586

