---
layout:    episode
title:    "Tough Times, Strong Relationships"
subtitle:  Special
date:      2018-02-25 23:09:19 +0900
itunes:    episode-23.m4a
opus:      episode-23.opus
chapters:  episode-23.vtt
length:    18940912
duration: "37:11"
---

Life is not always sunshine and roses, even when there's someone there to back 
us up. In the first episode of our two-part 2nd anniversary special, we discuss 
strategies couples can use to pull through hard times.

### Sources
[How Healthy Couples Handle Tough Times][tt], by Margarita Tartakovsky, M.S.  
[5 Surefire Ways to Sustain Your Relationship When Times Get Tough][5s], by Geri Alexander

[tt]: https://psychcentral.com/blog/how-healthy-couples-handle-tough-times
[5s]: https://www.huffingtonpost.com/geri-alexander/5-surefire-ways-to-sustain-your-relationship-when-times-get-tough_b_9538460.html

