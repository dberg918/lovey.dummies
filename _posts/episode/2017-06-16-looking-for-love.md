---
layout:    episode
title:    "Looking for Love"
subtitle:  Zach
date:      2017-06-16 15:03:13 +0900
itunes:    episode-15.m4a
opus:      episode-15.opus
chapters:  episode-15.vtt
length:    10610541
duration: "20:01"
---

Is love just a matter of fate, or is there a lot of work going on behind the 
scenes? Join us for Zach's take on this question as someone who is still looking 
for that special someone.
