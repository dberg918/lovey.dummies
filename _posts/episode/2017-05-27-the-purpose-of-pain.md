---
layout:    episode
title:    "The Purpose of Pain"
subtitle:  Date Night
date:      2017-05-27 14:16:40 +0900
itunes:    episode-14.m4a
opus:      episode-14.opus
chapters:  episode-14.vtt
length:    12893364
duration: "24:44"
---

Sometimes we don't understand the pain we experience, especially when it comes 
out of the blue and we're caught off guard. How do we close the gap between our 
thoughts and the reality that has just imposed itself upon us? Join us for date 
night in a frank discussion on pain, grief, and loss.
