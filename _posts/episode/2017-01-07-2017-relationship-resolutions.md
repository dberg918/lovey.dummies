---
layout:    episode
title:    "2017 Relationship Resolutions"
subtitle:  Standalone
date:      2017-01-07 10:25:00 +0900
itunes:    episode-8.mp3
opus:      episode-8.opus
length:    3848192
duration: "4:07"
---

It's a brand new year, and people everywhere are resolving to be better in their 
relationships. Do you have any resolutions for your relationships in 2017? 
Please [share them][1] with us!

[1]: http://loveydummies.com/contact
