---
layout:    episode
title:    "The Joys of Parenting"
subtitle:  Bill & Jenny
date:      2016-06-19 23:22:00 +0900
itunes:    episode-5.mp3
opus:      episode-5.opus
length:    28051456
duration: "38:34"
---

In tribute to mothers and fathers, join us as Dave talks to his parents about 
the important aspects of raising children.

#### Commentary sources
[*Fearless*][1], by Max Lucado

[1]: https://www.amazon.com/Fearless-Max-Lucado/dp/0849946395
