---
layout:    episode
title:    "Long-distance Love"
subtitle:  Peace & Cho
date:      2017-08-02 17:32:39 +0900
itunes:    episode-17.m4a
opus:      episode-17.opus
chapters:  episode-17.vtt
length:    20679541
duration: "40:46"
---

Many tread the territory, but few survive. What is the key to a successful long-
distance relationship? How do you maintain a connection with someone who is so 
far away? Join us as Peace and Yuhan (Cho) share how they made it all the way to 
matrimony.
