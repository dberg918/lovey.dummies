---
layout:    episode
title:    "Married Expat Life"
subtitle:  Troy & Gracia
date:      2018-10-14 22:17:56 +0900
itunes:    episode-27.m4a
opus:      episode-27.opus
chapters:  episode-27.vtt
length:    18131857
duration: "36:46"
---

What is it like to share the expat life with a special someone? Join us 
for this episode with our friends Troy and Gracia, who tell us about the 
joys and challenges of being overseas with a significant other.

### Tips for married expats
1. Create a home
2. If you like it, plan to stay. If you don't, plan to leave
3. Lower your expectations
4. Find your niche
