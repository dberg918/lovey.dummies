---
layout:    episode
title:    "Singleness"
subtitle:  Johnny & Matt
date:      2017-01-15 18:18:00 +0900
itunes:    episode-9.m4a
opus:      episode-9.opus
chapters:  episode-9.vtt
length:    33289611
duration: "44:01"
---

What's so important about being single? Join us as we talk with our friends 
Johnny and Matt about the single life, including how to prepare yourself 
for a future relationship and ways to keep things friendly with the opposite 
sex.

Have thoughts about being single? Please [share them][1] with us!

[1]: http://loveydummies.com/contact
