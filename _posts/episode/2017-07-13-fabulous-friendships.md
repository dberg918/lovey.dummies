---
layout:    episode
title:    "Fabulous Friendships"
subtitle:  Date Night
date:      2017-07-13 18:02:20 +0900
itunes:    episode-16.m4a
opus:      episode-16.opus
chapters:  episode-16.vtt
length:    10725653
duration: "20:16"
---

The book of Proverbs tells us that a man of many friends comes to ruin, but 
there is a friend who sticks closer than a brother. How important is friendship 
in our lives? Are you good at choosing, keeping, and forging friendships? In 
this episode, we take a closer look at our friendships and how to evaluate them 
in order to thrive.
