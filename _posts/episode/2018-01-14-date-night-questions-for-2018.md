---
layout:    episode
title:    "Date Night Questions for 2018"
subtitle:  Date Night
date:      2018-01-14 13:05:25 +0900
itunes:    episode-22.m4a
opus:      episode-22.opus
chapters:  episode-22.vtt
length:    14860618
duration: "28:47"
---

It's 2018! Join us for another date night episode where we talk about our 
intentions instead of New Year's resolutions.

Questions:
1. What would you try if you knew you could not fail?
2. What advice would you like to give yourself as you begin the New Year?
3. What would you be most happy about completing?
4. What is one change you could make to your lifestyle to give you more peace?
5. At the end of the year how would you like your life to be transformed?

Finish the sentence:
1. I want to repeat...
2. I want to gain...
3. I need more...
4. My helpers are...
5. I will succeed in...

### Source
[100 Process Questions to Bring in the New Year Mindfully][s], by CarlySullens

[s]: https://holidappy.com/holidays/100-Process-and-Survey-Questions-to-Bring-In-the-New-Year-Mindfully
