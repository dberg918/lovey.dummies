---
layout:    episode
title:    "Random Questions for Valentine's Date Night"
subtitle:  Standalone
date:      2017-02-13 12:00:00 +0900
itunes:    episode-10.m4a
opus:      episode-10.opus
chapters:  episode-10.vtt
length:    28480720
duration: "26:56"
---

It's almost that Lovey Dummy time of year! Join us for a Valentine's Date Night 
special where we go through a set of random questions. But there's a catch---we 
have to answer them for each other!

* Describe your dream home.
* If you could have one household appliance have a mind of its own to follow
your orders, which would you choose?
* If you could go back in time to any period in history, what point in time
would it be?
* If you were invisible for a day, what would you do?
* If you had to be an animal, which would you be?
* Name the top 3 dream jobs you'd like to have.
* If you had to live in another country (for always), where would you go?
* Imagine yourself as a car. Which model and make would you be? What color?
* If you were stuck on a desert island and could only have one kind of food for
the rest of your life, what would it be?
* What's the best advice you'd give yourself today?

Take the challenge with a friend or your fellow Lovey Dummy and [let us know][1]
how it goes!

[1]: http://loveydummies.com/contact
