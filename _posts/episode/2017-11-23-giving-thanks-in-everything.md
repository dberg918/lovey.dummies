---
layout:    episode
title:    "Giving Thanks in Everything"
subtitle:  Date Night
date:      2017-11-23 13:44:02 +0900
itunes:    episode-20.m4a
opus:      episode-20.opus
chapters:  episode-20.vtt
length:    11327396
duration: "21:31"
---

There's always something to be thankful for. On today's episode, we talk about 
the things we are grateful for this year. We'd also like to thank YOU&mdash;our 
loyal friends, listeners, and followers! Thanks for sticking with us!
