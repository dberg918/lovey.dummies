---
layout:    episode
title:    "Thriving in a Foreign Country"
subtitle:  Dave
date:      2017-10-14 22:13:26 +0900
itunes:    episode-19.m4a
opus:      episode-19.opus
chapters:  episode-19.vtt
length:    28744440
duration: "57:22"
---

The expat life can be exciting and refreshing, but finding "home" in a foreign 
land doesn't happen for everyone. Join us as we talk to Dave about his 
transition from playing saxophone on a cruise ship to running 
[Maverick English School][mav] in Japan. (Be sure to check it out and tell Dave 
we sent you!)

#### Commentary sources
[*Thriving in a Foreign Land*][thv], by Abigail Rogado

[mav]: https://maverickfukushima.wixsite.com
[thv]: https://journal.thriveglobal.com/thriving-in-a-foreign-land-5b0f4cb0987a
