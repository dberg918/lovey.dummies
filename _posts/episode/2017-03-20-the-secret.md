---
layout:    episode
title:    "The Secret"
subtitle:  Date Night
date:      2017-03-20 12:22:18 +0900
itunes:    episode-12.m4a
opus:      episode-12.opus
chapters:  episode-12.vtt
length:    34592003
duration: "33:05"
---

What is the secret to happy, healthy, and lasting relationships? How can we 
examine our performance in the relationships we're in? Are electrodes really 
necessary? Join us for another date night where we break down the most important 
aspect of any relationship and how we can put it into practice.

#### Sources
Emily Esfahani Smith, "[Masters of Love][1]", *The Atlantic*

[1]: https://www.theatlantic.com/health/archive/2014/06/happily-ever-after/372573/  
