---
layout:    episode
title:    "Out of This World"
subtitle:  Ben
date:      2018-04-30 21:26:42 +0900
itunes:    episode-25.m4a
opus:      episode-25.opus
chapters:  episode-25.vtt
length:    15653508
duration: "30:25"
---

As a Christian, where do you draw the boundary between being in the world, but 
not of the world? Join us for a discussion with our friend Ben, who is in the 
process of going into full-time Christian ministry.

[Support Ben!][sb] (Select Benjamin Shimomura as Designation)

[sb]: https://www.mustardseed.network/donate-osaka
