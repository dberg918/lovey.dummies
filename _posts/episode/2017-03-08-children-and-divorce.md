---
layout:    episode
title:    "Children and Divorce"
subtitle:  Kimberlin
date:      2017-03-08 10:57:47 +0900
itunes:    episode-11.m4a
opus:      episode-11.opus
chapters:  episode-11.vtt
length:    27293712
duration: "26:16"
---

What is it like to grow up in a single-parent household? Can children of 
divorced parents thrive? Divorce shouldn't be a life-long traumatic event. Join 
us as we have a heart-to-heart talk with Kimberlin about the effects of divorce 
on children and ways to cope with separation.

#### Commentary sources
[*Talking to Children About Divorce*][1], by Jean McBride

[1]: https://www.amazon.com/dp/B01AJ78M4E
