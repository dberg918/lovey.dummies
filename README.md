# Lovey Dummies
[loveydummies.com][1]  
Lovey Dummies is a podcast about relationships.

## The Show
We want to reach people who feel alone in their relational situations, no matter their marital status or religious background, by bringing them stories of relationships that they can connect with.

## The Hosts
Dave & Pearl are a couple who are currently living in Japan. Pearl is Filipino and Dave is American, so their different cultures and backgrounds are ultimately what led to this podcast.

## The Episodes
We want to present real people and real relationships in a way that everyone can learn something from them. We want to expose relational topics that aren’t normally discussed, and address cultural stereotypes in the arena of romantic relationships.

[1]: http://loveydummies.com
   "Lovey Dummies - A Relationship Podcast"
