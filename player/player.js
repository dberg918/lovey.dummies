function playToggle(playPause) {
    var id = playPause.parentElement.id.slice(8);
    chapters = document.getElementById("chapters" + id);
    controls = document.getElementById("controls" + id);
    audio = document.getElementById("audio" + id);

    shield = chapters.getElementsByClassName("shield")[0];
    previousChapter = chapters.getElementsByClassName("chapter-back")[0];
    nextChapter = chapters.getElementsByClassName("chapter-forward")[0];
    progress = chapters.getElementsByClassName("progress")[0];
    list = chapters.getElementsByClassName("list")[0];

    time = controls.getElementsByClassName("time")[0];
    backward = controls.getElementsByClassName("backward")[0];
    forward = controls.getElementsByClassName("forward")[0];

    if (backward.classList.contains("show") === false) {
        if (audio.textTracks.length > 0) {
            track = audio.lastElementChild;
            cues = track.track.cues;
            displayChapters(shield, list, audio, cues);
        } else {
            var item = document.createElement('li');
            item.innerHTML = "&#xf004; &#xf004; &#xf004;";
            list.appendChild(item);
            previousChapter.classList.add("hide");
            nextChapter.classList.add("hide");
            chapters.style.display = "block";
            shield.style.cursor = "default";
        }

        if (audio.readyState === 4) {
            loadTime(id);
        } else {
            time.children[0].innerHTML = "...";
            time.children[1].innerHTML = "...";
            audio.addEventListener("canplay", function() { loadTime(id) }, false);
        }
        backward.classList.add("show");
        forward.classList.add("show");
    }

    if (audio.paused) {
        if (audio.ended) {
            audio.currentTime = 0;
        }
        audio.addEventListener("timeupdate", function() { updateProgress(id) }, false);
        audio.addEventListener("ended", function() { playEnd(id) }, false);
        if (audio.textTracks.length > 0) {
            track.track.addEventListener("cuechange", function() { chapterWatch(id) }, false);
        }

		audio.play();
		playPause.outerHTML = '<i class="fa fa-pause-circle-o playpause" onclick="playToggle(this)"></i>';
    } else {
        audio.pause();
        playPause.outerHTML = '<i class="fa fa-play-circle-o playpause" onclick="playToggle(this)"></i>';
    }
}

function displayChapters(shield, list, audio, cues) {
    for (i=0; i < cues.length; i++) {
        var item = document.createElement('li');
        item.id = cues[i].id;
        var link = document.createElement('a');
        link.href = '#' + audio.id;
        link.innerHTML = cues[i].text;
        link.setAttribute('data-chapter', i);
        link.onclick = function () {
            seekChapter(audio, this.getAttribute('data-chapter'));
            toggleList(shield);
        }
        item.appendChild(link);
        list.appendChild(item);
    }
    chapters.style.display = "block";
}

function toggleList(shield) {
    chapters = shield.parentElement;
    previousChapter = shield.nextElementSibling;
    nextChapter = previousChapter.nextElementSibling;
    progress = nextChapter.nextElementSibling;
    list = progress.nextElementSibling;

    if (list.childNodes.length > 1) {
        chapters.classList.toggle("seek");
        shield.classList.toggle("seek");
        previousChapter.classList.toggle("seek");
        nextChapter.classList.toggle("seek");
        progress.classList.toggle("seek");
        list.classList.toggle("seek");
    }
}

function seekChapter(audio, seekch) {
    if (seekch) {
        var id = audio.id.slice(5);
        cues = audio.lastElementChild.track.cues;
        audio.currentTime = cues[seekch].startTime;
        updateProgress(id);
        chapterWatch(id);
    }
}

function chapterBackward (previousChapter) {
    var id = previousChapter.parentElement.id.slice(8);
    audio = document.getElementById("audio" + id);
    track = audio.lastElementChild.track;
    cues = track.cues;
    var currentIndex = track.activeCues[0].id - 1;
    var skipLimit = cues[currentIndex].startTime + 3;

    if (audio.currentTime > skipLimit) {
        audio.currentTime = cues[currentIndex].startTime;
        updateProgress(id);
    } else {
        var previousIndex = currentIndex - 1;
        if (previousIndex > -1) {
            audio.currentTime = cues[previousIndex].startTime;
            updateProgress(id);
            chapterWatch(id);
        }
    }
}

function chapterForward (nextChapter) {
    var id = nextChapter.parentElement.id.slice(8);
    audio = document.getElementById("audio" + id);
    track = audio.lastElementChild.track;
    cues = track.cues;
    var nextIndex = track.activeCues[0].id;
    if (nextIndex < (track.cues.length)) {
            audio.currentTime = cues[nextIndex].startTime;
            updateProgress(id);
            chapterWatch(id);
    }
}

function skipBackward(backward) {
    var id = backward.parentElement.id.slice(8);
    audio = document.getElementById("audio" + id);
    audio.currentTime -= 15;
    updateProgress(id);
    if (audio.textTracks.length > 0) {
        chapterWatch(id);
    }
}

function skipForward(forward) {
    var id = forward.parentElement.id.slice(8);
    audio = document.getElementById("audio" + id);
    audio.currentTime += 15;
    updateProgress(id);
    if (audio.textTracks.length > 0) {
        chapterWatch(id);
    }
}

function loadTime(id) {
    audio = document.getElementById("audio" + id);
    controls = document.getElementById("controls" + id);
    time = controls.children[0];
    var totalSeconds = Math.floor(audio.duration);
    var minutes = Math.floor(totalSeconds / 60);
    var seconds = Math.floor(totalSeconds % 60);
    time.children[0].innerHTML = "00:00";
    time.children[1].innerHTML = ('0'+minutes).slice(-2) + ':' + ('0'+seconds).slice(-2);
}

function updateProgress(id) {
    audio = document.getElementById("audio" + id);
    progress = document.getElementById("progress" + id);
    controls = document.getElementById("controls" + id);
    time = controls.children[0];
    var value = 0;
    if (audio.currentTime > 0) {
        var totalSeconds = audio.currentTime;
        var minutes = Math.floor(totalSeconds / 60);
        var seconds = Math.floor(totalSeconds % 60);
        time.children[0].innerHTML = ('0'+minutes).slice(-2) +':'+ ('0'+seconds).slice(-2);
        value = (100 / audio.duration) * audio.currentTime;
    }
    progress.style.width = value + "%";
}

function chapterWatch(id) {
    audio = document.getElementById("audio" + id);
    list = document.getElementById("cl" + id);
    track = audio.lastElementChild;
    var dataChapter = Number(track.track.activeCues[0].id) - 1;
    var top = (dataChapter * -2) - 2;
    list.style.top = top + "rem";
}

function playEnd(id) {
    controls = document.getElementById("controls" + id);
    playPause = controls.getElementsByClassName("playpause")[0];
    playPause.outerHTML = '<i class="fa fa-play-circle-o playpause" onclick="playToggle(this)"></i>';
}
