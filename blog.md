---
layout: default
title: Blog
---

<section class="posts container">

{% for post in site.posts %}{% if post.layout == "blog" %}
<article class="post">
  <div class="post-header" href="{{ site.baseurl }}{{ post.url }}">
    <div class="post-cover" style="background: url('{{ site.s3_baseurl }}/blog/{{ post.id | remove_first:'/' | replace:'/','-' }}.jpg') center/cover no-repeat;"></div>
    <div class="post-float">
      <h2 class="post-title">
        {{ post.title }}
      </h2>
      <time datetime="{{ post.date | date_to_xmlschema }}"
          class="post-date">{{ post.date | date: "%B %-d, %Y" }}</time>
    </div>
  </div>
  <div class="post-body">
    {{ post.excerpt }}
  </div>
  <a class="download" href="{{ site.baseurl }}{{ post.url }}">
    <span class="icon">&#xf0a4;</span> Read more...
  </a>
</article>
{% endif %}{% endfor %}

</section>
